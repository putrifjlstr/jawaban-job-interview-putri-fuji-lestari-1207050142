# Jawaban Job Interview Putri Fuji Lestari-1207050142



## Demo Deteksi Masker

![Demo Program](https://gitlab.com/putrifjlstr/jawaban-job-interview-putri-fuji-lestari-1207050142/-/blob/main/Deteksi%20Masker/deteksi-masker-2of2.gif)

## Library yang dipakai

pada program Deteksi Masker ini menggunakan library OpenCV untuk mendeteksi wajah dan hidung pada gambar atau frame video.

`cv2.CascadeClassifier()` adalah sebuah kelas OpenCV yang sudah dibuat sebelumnya yang digunakan untuk mendeteksi objek dengan menggunakan Haar Cascade Classifiers.

`'haarcascade_frontalface_default.xml'` dan `'haarcascade_mcs_nose.xml'` adalah dua classifier terlatih yang digunakan oleh kelas `CascadeClassifier` untuk mendeteksi wajah dan hidung secara berurutan. Classifier ini berisi serangkaian fitur yang digunakan untuk mengidentifikasi objek target pada gambar masukan.

pada program ini, kode menginisialisasi dua instance dari kelas `CascadeClassifier`, satu untuk deteksi wajah dan satu lagi untuk deteksi hidung, dan menetapkan keduanya ke variabel `faceCascade` dan `noseCascade`.

Classifier ini pada objek target (wajah atau hidung) digunakan untuk mendeteksi dengan akurasi pada kondisi yang berbeda. Classifier menggunakan set fitur Haar dan algoritma pembelajaran mesin untuk mengidentifikasi objek pada gambar masukan.

Setelah objek `CascadeClassifier` diinisialisasi, program dapat digunakan untuk mendeteksi wajah dan hidung pada gambar masukan atau frame video menggunakan metode `detectMultiScale()` dari kelas `CascadeClassifier`. Metode `detectMultiScale()` mengembalikan daftar persegi panjang yang merepresentasikan kotak pembatas dari objek yang terdeteksi.



## Demo program Subtitel Otomatis

[Demo Program](https://gitlab.com/putrifjlstr/jawaban-job-interview-putri-fuji-lestari-1207050142/-/blob/main/Subtitle%20Otomatis/capture_demo_apk.mp4)

## Cara Penerapan file subtitel (format srt) pada video
[Penerapan Subtitel](https://gitlab.com/putrifjlstr/jawaban-job-interview-putri-fuji-lestari-1207050142/-/blob/main/Subtitle%20Otomatis/cara_menerapkan_subtitel_ke_vid.mp4)


## Library yang dipakai

1. `speech_recognition` adalah library Python yang digunakan untuk melakukan speech recognition atau pengenalan suara. Library ini dapat digunakan untuk mengubah audio suara menjadi teks yang dapat dibaca oleh komputer. Di sini, library ini diimport dengan alias `sr` dan digunakan untuk mengubah file audio menjadi teks.

2. `pydub` adalah library Python yang digunakan untuk memanipulasi file audio. Library ini dapat digunakan untuk memotong, menyambung, dan memodifikasi berbagai aspek file audio, seperti format dan bitrate. Di sini, library ini digunakan untuk memuat dan memanipulasi file audio sebelum dikonversi menjadi teks oleh library `speech_recognition`.

3. `srt` adalah library Python yang digunakan untuk memanipulasi file subtitle. Library ini dapat digunakan untuk membaca, menulis, dan memodifikasi berbagai aspek file subtitle, seperti timing dan teks subtitle. Di sini, library ini digunakan untuk membuat file subtitle baru yang berisi teks hasil transkripsi dari file audio.

4. `datetime` adalah library Python yang digunakan untuk memanipulasi tanggal dan waktu. Library ini dapat digunakan untuk melakukan berbagai operasi pada tanggal dan waktu, seperti menghitung selisih waktu, memformat tanggal, dan sebagainya. Di sini, library ini digunakan untuk memanipulasi waktu dalam file subtitle.




