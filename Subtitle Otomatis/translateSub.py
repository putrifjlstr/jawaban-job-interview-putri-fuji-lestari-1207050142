import srt
from googletrans import Translator, LANGUAGES

# Baca file subtitle asli
with open('subtitel1.srt', 'r') as f:
    subtitles = list(srt.parse(f.read()))

# Tentukan bahasa tujuan
target_language = 'en'

# Buat objek translator
translator = Translator()

# Terjemahkan setiap subtitle
for subtitle in subtitles:
    original_text = subtitle.content
    translation = translator.translate(original_text, dest= target_language).text
    subtitle.content = translation

# Simpan subtitle terjemahan ke dalam file
with open('subtitle_terjemahan.srt', 'w') as f:
    f.write(srt.compose(subtitles))

